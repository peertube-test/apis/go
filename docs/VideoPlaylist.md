# VideoPlaylist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Uuid** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**IsLocal** | **bool** |  | [optional] 
**VideoLength** | **float32** |  | [optional] 
**ThumbnailPath** | **string** |  | [optional] 
**Privacy** | [**VideoPlaylistPrivacy**](VideoPlaylist_privacy.md) |  | [optional] 
**Type** | [**VideoPlaylistPrivacy**](VideoPlaylist_privacy.md) |  | [optional] 
**OwnerAccount** | [**VideoPlaylistOwnerAccount**](VideoPlaylist_ownerAccount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


