# Follow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Follower** | [**Actor**](Actor.md) |  | [optional] 
**Following** | [**Actor**](Actor.md) |  | [optional] 
**Score** | **float32** |  | [optional] 
**State** | **string** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


