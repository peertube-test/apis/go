# ServerConfigAvatar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**File** | [**ServerConfigAvatarFile**](ServerConfig_avatar_file.md) |  | [optional] 
**Extensions** | **[]string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


