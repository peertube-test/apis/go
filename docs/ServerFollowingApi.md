# \ServerFollowingApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ServerFollowersGet**](ServerFollowingApi.md#ServerFollowersGet) | **Get** /server/followers | Get followers of the server
[**ServerFollowingGet**](ServerFollowingApi.md#ServerFollowingGet) | **Get** /server/following | Get servers followed by the server
[**ServerFollowingHostDelete**](ServerFollowingApi.md#ServerFollowingHostDelete) | **Delete** /server/following/{host} | Unfollow a server by hostname
[**ServerFollowingPost**](ServerFollowingApi.md#ServerFollowingPost) | **Post** /server/following | Follow a server



## ServerFollowersGet

> []Follow ServerFollowersGet(ctx, optional)

Get followers of the server

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ServerFollowersGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ServerFollowersGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**[]Follow**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingGet

> []Follow ServerFollowingGet(ctx, optional)

Get servers followed by the server

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ServerFollowingGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ServerFollowingGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**[]Follow**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingHostDelete

> ServerFollowingHostDelete(ctx, host)

Unfollow a server by hostname

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**host** | **string**| The host to unfollow  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ServerFollowingPost

> ServerFollowingPost(ctx, optional)

Follow a server

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***ServerFollowingPostOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ServerFollowingPostOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **follow** | [**optional.Interface of Follow**](Follow.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

