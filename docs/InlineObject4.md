# InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Torrentfile** | [***os.File**](*os.File.md) | Torrent File | [optional] 
**TargetUrl** | **string** | HTTP target URL | [optional] 
**MagnetUri** | **string** | Magnet URI | [optional] 
**ChannelId** | **float32** | Channel id that will contain this video | 
**Thumbnailfile** | [***os.File**](*os.File.md) | Video thumbnail file | [optional] 
**Previewfile** | [***os.File**](*os.File.md) | Video preview file | [optional] 
**Privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**Category** | **string** | Video category | [optional] 
**Licence** | **string** | Video licence | [optional] 
**Language** | **string** | Video language | [optional] 
**Description** | **string** | Video description | [optional] 
**WaitTranscoding** | **string** | Whether or not we wait transcoding before publish the video | [optional] 
**Support** | **string** | Text describing how to support the video uploader | [optional] 
**Nsfw** | **string** | Whether or not this video contains sensitive content | [optional] 
**Name** | **string** | Video name | 
**Tags** | **[]string** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**CommentsEnabled** | **string** | Enable or disable comments for this video | [optional] 
**ScheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


