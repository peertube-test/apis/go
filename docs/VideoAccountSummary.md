# VideoAccountSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Name** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**Url** | **string** |  | [optional] 
**Host** | **string** |  | [optional] 
**Avatar** | [**Avatar**](Avatar.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


