# \AccountsApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountsGet**](AccountsApi.md#AccountsGet) | **Get** /accounts | Get all accounts
[**AccountsNameGet**](AccountsApi.md#AccountsNameGet) | **Get** /accounts/{name} | Get the account by name
[**AccountsNameVideosGet**](AccountsApi.md#AccountsNameVideosGet) | **Get** /accounts/{name}/videos | Get videos for an account, provided the name of that account



## AccountsGet

> []Account AccountsGet(ctx, optional)

Get all accounts

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***AccountsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AccountsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**[]Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AccountsNameGet

> Account AccountsNameGet(ctx, name)

Get the account by name

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AccountsNameVideosGet

> VideoListResponse AccountsNameVideosGet(ctx, name)

Get videos for an account, provided the name of that account

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

