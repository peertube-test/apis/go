# ServerConfigVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Image** | [**ServerConfigVideoImage**](ServerConfig_video_image.md) |  | [optional] 
**File** | [**ServerConfigVideoFile**](ServerConfig_video_file.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


