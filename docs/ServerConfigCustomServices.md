# ServerConfigCustomServices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Twitter** | [**ServerConfigCustomServicesTwitter**](ServerConfigCustom_services_twitter.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


