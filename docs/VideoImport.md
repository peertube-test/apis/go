# VideoImport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**TargetUrl** | **string** |  | [optional] 
**MagnetUri** | **string** |  | [optional] 
**TorrentName** | **string** |  | [optional] 
**State** | [**VideoImportState**](VideoImport_state.md) |  | [optional] 
**Error** | **string** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


