# User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Username** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**DisplayNSFW** | **bool** |  | [optional] 
**AutoPlayVideo** | **bool** |  | [optional] 
**Role** | **int32** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | [optional] 
**RoleLabel** | **string** |  | [optional] 
**VideoQuota** | **float32** |  | [optional] 
**VideoQuotaDaily** | **float32** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**Account** | [**Account**](Account.md) |  | [optional] 
**VideoChannels** | [**[]VideoChannel**](VideoChannel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


