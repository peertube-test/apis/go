# \SearchApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SearchVideosGet**](SearchApi.md#SearchVideosGet) | **Get** /search/videos | Get the videos corresponding to a given query



## SearchVideosGet

> VideoListResponse SearchVideosGet(ctx, search, optional)

Get the videos corresponding to a given query

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**search** | **string**| String to search | 
 **optional** | ***SearchVideosGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SearchVideosGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

