# AddUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | The user username  | 
**Password** | **string** | The user password  | 
**Email** | **string** | The user email  | 
**VideoQuota** | **string** | The user videoQuota  | 
**VideoQuotaDaily** | **string** | The user daily video quota  | 
**Role** | **int32** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


