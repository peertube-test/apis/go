# Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Url** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Host** | **string** |  | [optional] 
**FollowingCount** | **float32** |  | [optional] 
**FollowersCount** | **float32** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Avatar** | [**Avatar**](Avatar.md) |  | [optional] 
**UserId** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


