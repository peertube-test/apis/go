# Go API client for PeerTube

This Python package is automatically generated from [PeerTube's REST API](https://docs.joinpeertube.org/api-rest-reference.html),
using the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 2.1.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

For more information, please visit [https://joinpeertube.org](https://joinpeertube.org)

## Installation

Install the following dependencies:

```shell
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
go get github.com/antihax/optional
```

Put the package under your project folder and add the following in import:

```golang
import "./peertube"
```

## Documentation for API Endpoints

All URIs are relative to *https://peertube.cpy.re/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**AccountsGet**](docs/AccountsApi.md#accountsget) | **Get** /accounts | Get all accounts
*AccountsApi* | [**AccountsNameGet**](docs/AccountsApi.md#accountsnameget) | **Get** /accounts/{name} | Get the account by name
*AccountsApi* | [**AccountsNameVideosGet**](docs/AccountsApi.md#accountsnamevideosget) | **Get** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*ConfigApi* | [**ConfigAboutGet**](docs/ConfigApi.md#configaboutget) | **Get** /config/about | Get the instance about page content
*ConfigApi* | [**ConfigCustomDelete**](docs/ConfigApi.md#configcustomdelete) | **Delete** /config/custom | Delete the runtime configuration of the server
*ConfigApi* | [**ConfigCustomGet**](docs/ConfigApi.md#configcustomget) | **Get** /config/custom | Get the runtime configuration of the server
*ConfigApi* | [**ConfigCustomPut**](docs/ConfigApi.md#configcustomput) | **Put** /config/custom | Set the runtime configuration of the server
*ConfigApi* | [**ConfigGet**](docs/ConfigApi.md#configget) | **Get** /config | Get the public configuration of the server
*JobApi* | [**JobsStateGet**](docs/JobApi.md#jobsstateget) | **Get** /jobs/{state} | Get list of jobs
*MyUserApi* | [**UsersMeAvatarPickPost**](docs/MyUserApi.md#usersmeavatarpickpost) | **Post** /users/me/avatar/pick | Update current user avatar
*MyUserApi* | [**UsersMeGet**](docs/MyUserApi.md#usersmeget) | **Get** /users/me | Get current user information
*MyUserApi* | [**UsersMePut**](docs/MyUserApi.md#usersmeput) | **Put** /users/me | Update current user information
*MyUserApi* | [**UsersMeSubscriptionsExistGet**](docs/MyUserApi.md#usersmesubscriptionsexistget) | **Get** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
*MyUserApi* | [**UsersMeSubscriptionsGet**](docs/MyUserApi.md#usersmesubscriptionsget) | **Get** /users/me/subscriptions | Get subscriptions of the current user
*MyUserApi* | [**UsersMeSubscriptionsPost**](docs/MyUserApi.md#usersmesubscriptionspost) | **Post** /users/me/subscriptions | Add subscription to the current user
*MyUserApi* | [**UsersMeSubscriptionsSubscriptionHandleDelete**](docs/MyUserApi.md#usersmesubscriptionssubscriptionhandledelete) | **Delete** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
*MyUserApi* | [**UsersMeSubscriptionsSubscriptionHandleGet**](docs/MyUserApi.md#usersmesubscriptionssubscriptionhandleget) | **Get** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
*MyUserApi* | [**UsersMeSubscriptionsVideosGet**](docs/MyUserApi.md#usersmesubscriptionsvideosget) | **Get** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
*MyUserApi* | [**UsersMeVideoQuotaUsedGet**](docs/MyUserApi.md#usersmevideoquotausedget) | **Get** /users/me/video-quota-used | Get current user used quota
*MyUserApi* | [**UsersMeVideosGet**](docs/MyUserApi.md#usersmevideosget) | **Get** /users/me/videos | Get videos of the current user
*MyUserApi* | [**UsersMeVideosImportsGet**](docs/MyUserApi.md#usersmevideosimportsget) | **Get** /users/me/videos/imports | Get video imports of current user
*MyUserApi* | [**UsersMeVideosVideoIdRatingGet**](docs/MyUserApi.md#usersmevideosvideoidratingget) | **Get** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user
*SearchApi* | [**SearchVideosGet**](docs/SearchApi.md#searchvideosget) | **Get** /search/videos | Get the videos corresponding to a given query
*ServerFollowingApi* | [**ServerFollowersGet**](docs/ServerFollowingApi.md#serverfollowersget) | **Get** /server/followers | Get followers of the server
*ServerFollowingApi* | [**ServerFollowingGet**](docs/ServerFollowingApi.md#serverfollowingget) | **Get** /server/following | Get servers followed by the server
*ServerFollowingApi* | [**ServerFollowingHostDelete**](docs/ServerFollowingApi.md#serverfollowinghostdelete) | **Delete** /server/following/{host} | Unfollow a server by hostname
*ServerFollowingApi* | [**ServerFollowingPost**](docs/ServerFollowingApi.md#serverfollowingpost) | **Post** /server/following | Follow a server
*UserApi* | [**AccountsNameRatingsGet**](docs/UserApi.md#accountsnameratingsget) | **Get** /accounts/{name}/ratings | Get ratings of an account by its name
*UserApi* | [**UsersGet**](docs/UserApi.md#usersget) | **Get** /users | Get a list of users
*UserApi* | [**UsersIdDelete**](docs/UserApi.md#usersiddelete) | **Delete** /users/{id} | Delete a user by its id
*UserApi* | [**UsersIdGet**](docs/UserApi.md#usersidget) | **Get** /users/{id} | Get user by its id
*UserApi* | [**UsersIdPut**](docs/UserApi.md#usersidput) | **Put** /users/{id} | Update user profile by its id
*UserApi* | [**UsersPost**](docs/UserApi.md#userspost) | **Post** /users | Creates user
*UserApi* | [**UsersRegisterPost**](docs/UserApi.md#usersregisterpost) | **Post** /users/register | Register a user
*VideoApi* | [**AccountsNameVideosGet**](docs/VideoApi.md#accountsnamevideosget) | **Get** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*VideoApi* | [**VideoChannelsChannelHandleVideosGet**](docs/VideoApi.md#videochannelschannelhandlevideosget) | **Get** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoApi* | [**VideosCategoriesGet**](docs/VideoApi.md#videoscategoriesget) | **Get** /videos/categories | Get list of video categories known by the server
*VideoApi* | [**VideosGet**](docs/VideoApi.md#videosget) | **Get** /videos | Get list of videos
*VideoApi* | [**VideosIdDelete**](docs/VideoApi.md#videosiddelete) | **Delete** /videos/{id} | Delete a video by its id
*VideoApi* | [**VideosIdDescriptionGet**](docs/VideoApi.md#videosiddescriptionget) | **Get** /videos/{id}/description | Get a video description by its id
*VideoApi* | [**VideosIdGet**](docs/VideoApi.md#videosidget) | **Get** /videos/{id} | Get a video by its id
*VideoApi* | [**VideosIdGiveOwnershipPost**](docs/VideoApi.md#videosidgiveownershippost) | **Post** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
*VideoApi* | [**VideosIdPut**](docs/VideoApi.md#videosidput) | **Put** /videos/{id} | Update metadata for a video by its id
*VideoApi* | [**VideosIdViewsPost**](docs/VideoApi.md#videosidviewspost) | **Post** /videos/{id}/views | Add a view to the video by its id
*VideoApi* | [**VideosIdWatchingPut**](docs/VideoApi.md#videosidwatchingput) | **Put** /videos/{id}/watching | Set watching progress of a video by its id for a user
*VideoApi* | [**VideosImportsPost**](docs/VideoApi.md#videosimportspost) | **Post** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
*VideoApi* | [**VideosLanguagesGet**](docs/VideoApi.md#videoslanguagesget) | **Get** /videos/languages | Get list of languages known by the server
*VideoApi* | [**VideosLicencesGet**](docs/VideoApi.md#videoslicencesget) | **Get** /videos/licences | Get list of video licences known by the server
*VideoApi* | [**VideosOwnershipGet**](docs/VideoApi.md#videosownershipget) | **Get** /videos/ownership | Get list of video ownership changes requests
*VideoApi* | [**VideosOwnershipIdAcceptPost**](docs/VideoApi.md#videosownershipidacceptpost) | **Post** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
*VideoApi* | [**VideosOwnershipIdRefusePost**](docs/VideoApi.md#videosownershipidrefusepost) | **Post** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
*VideoApi* | [**VideosPrivaciesGet**](docs/VideoApi.md#videosprivaciesget) | **Get** /videos/privacies | Get list of privacy policies supported by the server
*VideoApi* | [**VideosUploadPost**](docs/VideoApi.md#videosuploadpost) | **Post** /videos/upload | Upload a video file with its metadata
*VideoAbuseApi* | [**VideosAbuseGet**](docs/VideoAbuseApi.md#videosabuseget) | **Get** /videos/abuse | Get list of reported video abuses
*VideoAbuseApi* | [**VideosIdAbusePost**](docs/VideoAbuseApi.md#videosidabusepost) | **Post** /videos/{id}/abuse | Report an abuse, on a video by its id
*VideoBlacklistApi* | [**VideosBlacklistGet**](docs/VideoBlacklistApi.md#videosblacklistget) | **Get** /videos/blacklist | Get list of videos on blacklist
*VideoBlacklistApi* | [**VideosIdBlacklistDelete**](docs/VideoBlacklistApi.md#videosidblacklistdelete) | **Delete** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
*VideoBlacklistApi* | [**VideosIdBlacklistPost**](docs/VideoBlacklistApi.md#videosidblacklistpost) | **Post** /videos/{id}/blacklist | Put on blacklist a video by its id
*VideoCaptionApi* | [**VideosIdCaptionsCaptionLanguageDelete**](docs/VideoCaptionApi.md#videosidcaptionscaptionlanguagedelete) | **Delete** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionApi* | [**VideosIdCaptionsCaptionLanguagePut**](docs/VideoCaptionApi.md#videosidcaptionscaptionlanguageput) | **Put** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionApi* | [**VideosIdCaptionsGet**](docs/VideoCaptionApi.md#videosidcaptionsget) | **Get** /videos/{id}/captions | Get list of video&#39;s captions
*VideoChannelApi* | [**AccountsNameVideoChannelsGet**](docs/VideoChannelApi.md#accountsnamevideochannelsget) | **Get** /accounts/{name}/video-channels | Get video channels of an account by its name
*VideoChannelApi* | [**VideoChannelsChannelHandleDelete**](docs/VideoChannelApi.md#videochannelschannelhandledelete) | **Delete** /video-channels/{channelHandle} | Delete a video channel by its id
*VideoChannelApi* | [**VideoChannelsChannelHandleGet**](docs/VideoChannelApi.md#videochannelschannelhandleget) | **Get** /video-channels/{channelHandle} | Get a video channel by its id
*VideoChannelApi* | [**VideoChannelsChannelHandlePut**](docs/VideoChannelApi.md#videochannelschannelhandleput) | **Put** /video-channels/{channelHandle} | Update a video channel by its id
*VideoChannelApi* | [**VideoChannelsChannelHandleVideosGet**](docs/VideoChannelApi.md#videochannelschannelhandlevideosget) | **Get** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoChannelApi* | [**VideoChannelsGet**](docs/VideoChannelApi.md#videochannelsget) | **Get** /video-channels | Get list of video channels
*VideoChannelApi* | [**VideoChannelsPost**](docs/VideoChannelApi.md#videochannelspost) | **Post** /video-channels | Creates a video channel for the current user
*VideoCommentApi* | [**VideosIdCommentThreadsGet**](docs/VideoCommentApi.md#videosidcommentthreadsget) | **Get** /videos/{id}/comment-threads | Get the comment threads of a video by its id
*VideoCommentApi* | [**VideosIdCommentThreadsPost**](docs/VideoCommentApi.md#videosidcommentthreadspost) | **Post** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
*VideoCommentApi* | [**VideosIdCommentThreadsThreadIdGet**](docs/VideoCommentApi.md#videosidcommentthreadsthreadidget) | **Get** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
*VideoCommentApi* | [**VideosIdCommentsCommentIdDelete**](docs/VideoCommentApi.md#videosidcommentscommentiddelete) | **Delete** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
*VideoCommentApi* | [**VideosIdCommentsCommentIdPost**](docs/VideoCommentApi.md#videosidcommentscommentidpost) | **Post** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id
*VideoPlaylistApi* | [**VideoPlaylistsGet**](docs/VideoPlaylistApi.md#videoplaylistsget) | **Get** /video-playlists | Get list of video playlists
*VideoRateApi* | [**VideosIdRatePut**](docs/VideoRateApi.md#videosidrateput) | **Put** /videos/{id}/rate | Vote for a video by its id


## Documentation For Models

 - [Account](docs/Account.md)
 - [Actor](docs/Actor.md)
 - [AddUser](docs/AddUser.md)
 - [AddUserResponse](docs/AddUserResponse.md)
 - [Avatar](docs/Avatar.md)
 - [CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [CommentThreadResponse](docs/CommentThreadResponse.md)
 - [Follow](docs/Follow.md)
 - [GetMeVideoRating](docs/GetMeVideoRating.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineObject2](docs/InlineObject2.md)
 - [InlineObject3](docs/InlineObject3.md)
 - [InlineObject4](docs/InlineObject4.md)
 - [InlineObject5](docs/InlineObject5.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [Job](docs/Job.md)
 - [PlaylistElement](docs/PlaylistElement.md)
 - [RegisterUser](docs/RegisterUser.md)
 - [RegisterUserChannel](docs/RegisterUserChannel.md)
 - [ServerConfig](docs/ServerConfig.md)
 - [ServerConfigAbout](docs/ServerConfigAbout.md)
 - [ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [ServerConfigCustom](docs/ServerConfigCustom.md)
 - [ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [ServerConfigEmail](docs/ServerConfigEmail.md)
 - [ServerConfigImport](docs/ServerConfigImport.md)
 - [ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [ServerConfigInstance](docs/ServerConfigInstance.md)
 - [ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [ServerConfigSignup](docs/ServerConfigSignup.md)
 - [ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [ServerConfigTrending](docs/ServerConfigTrending.md)
 - [ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [ServerConfigUser](docs/ServerConfigUser.md)
 - [ServerConfigVideo](docs/ServerConfigVideo.md)
 - [ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [UpdateMe](docs/UpdateMe.md)
 - [UpdateUser](docs/UpdateUser.md)
 - [User](docs/User.md)
 - [UserWatchingVideo](docs/UserWatchingVideo.md)
 - [Video](docs/Video.md)
 - [VideoAbuse](docs/VideoAbuse.md)
 - [VideoAbuseVideo](docs/VideoAbuseVideo.md)
 - [VideoAccountSummary](docs/VideoAccountSummary.md)
 - [VideoBlacklist](docs/VideoBlacklist.md)
 - [VideoCaption](docs/VideoCaption.md)
 - [VideoChannel](docs/VideoChannel.md)
 - [VideoChannelCreate](docs/VideoChannelCreate.md)
 - [VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [VideoChannelSummary](docs/VideoChannelSummary.md)
 - [VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [VideoComment](docs/VideoComment.md)
 - [VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [VideoConstantNumber](docs/VideoConstantNumber.md)
 - [VideoConstantString](docs/VideoConstantString.md)
 - [VideoDetails](docs/VideoDetails.md)
 - [VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [VideoFile](docs/VideoFile.md)
 - [VideoImport](docs/VideoImport.md)
 - [VideoImportState](docs/VideoImportState.md)
 - [VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [VideoListResponse](docs/VideoListResponse.md)
 - [VideoPlaylist](docs/VideoPlaylist.md)
 - [VideoPlaylistOwnerAccount](docs/VideoPlaylistOwnerAccount.md)
 - [VideoPlaylistPrivacy](docs/VideoPlaylistPrivacy.md)
 - [VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [VideoPrivacySet](docs/VideoPrivacySet.md)
 - [VideoRating](docs/VideoRating.md)
 - [VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [VideoStateConstant](docs/VideoStateConstant.md)
 - [VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [VideoStreamingPlaylistsRedundancies](docs/VideoStreamingPlaylistsRedundancies.md)
 - [VideoUploadResponse](docs/VideoUploadResponse.md)
 - [VideoUserHistory](docs/VideoUserHistory.md)


## Documentation For Authorization



## OAuth2


- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
 - **admin**: Admin scope
 - **moderator**: Moderator scope
 - **user**: User scope

Example

```golang
auth := context.WithValue(context.Background(), sw.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```golang
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, sw.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.
