/*
 * PeerTube
 *
 * # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 
 *
 * API version: 2.1.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package peertube
// Account struct for Account
type Account struct {
	Id float32 `json:"id,omitempty"`
	Url string `json:"url,omitempty"`
	Name string `json:"name,omitempty"`
	Host string `json:"host,omitempty"`
	FollowingCount float32 `json:"followingCount,omitempty"`
	FollowersCount float32 `json:"followersCount,omitempty"`
	CreatedAt string `json:"createdAt,omitempty"`
	UpdatedAt string `json:"updatedAt,omitempty"`
	Avatar Avatar `json:"avatar,omitempty"`
	UserId string `json:"userId,omitempty"`
	DisplayName string `json:"displayName,omitempty"`
	Description string `json:"description,omitempty"`
}
